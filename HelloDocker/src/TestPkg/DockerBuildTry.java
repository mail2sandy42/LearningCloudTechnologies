package TestPkg;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class DockerBuildTry {

	public static void main(String[] args) {
		String output = "";
		
		if (args != null && args.length > 0) {
			output = "You have " + args.length + " arguments\n";
			for (String prms : args)
				output += prms + "\n";
		} else {
			output = "This is a docker try application for test"+ "\n";
		}

		//just print
		System.out.print(output);
		System.out.print(WriteToFile(output) + "\n");
		
		/*while(true){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
	}

	private static String WriteToFile(String output) {
		try{
			String path = "YourLastResult_Log.txt";

			String abs = new File(path).getAbsolutePath();
			
			BufferedWriter out = new BufferedWriter(new FileWriter(path));
            out.write(output);
            out.close();
	        return "Your output stored in " + abs;
		}catch(Exception ex){
			return "Failed to write the log dut to exception - " + ex.getMessage();
		}		
	}

}
