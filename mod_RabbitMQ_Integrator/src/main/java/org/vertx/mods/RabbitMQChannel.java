/*
 * Copyright 2011-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vertx.mods;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

import java.io.*;
import static com.google.common.base.Strings.isNullOrEmpty;

public class RabbitMQChannel extends Verticle {

	private Handler<Message<JsonObject>> handler;

	@Override
	public void start() {

		// extract rabbitMQ configuration
		JsonObject rabbitMQConfig = container.config().getObject(
				"rabbitMQ_Settings", new JsonObject());
		String rabbitMQServerIp = rabbitMQConfig.getString("rbServerUrl");
		String rabbit_exchangeName = rabbitMQConfig.getString("exchangeName");
		String rabbit_exchangeType = rabbitMQConfig.getString("exchangeType");
		int rabbit_port = GetPort(rabbitMQConfig.getString("port"));
		// extract rabbitMQ configuration

		// extract mqtt configuration
		JsonObject mqttConfig = container.config().getObject("mqtt_channel",
				new JsonObject());
		String topicIn = mqttConfig.getString("topic_in");// value in json is
															// mqtt.01
		String topicApplication = mqttConfig.getString("topic_out_application");
		// extract mqtt configuration

		// define the handler & register to get messages from vertx event bus
		handler = new RabbitMQSender(rabbitMQServerIp, rabbit_port,
				rabbit_exchangeName, rabbit_exchangeType, topicApplication);
		vertx.eventBus().registerHandler(topicIn, handler);
		// define the handler & register to get messages from vertx event bus

		System.out
				.println("RabbitMQ initialized to send messages to - Exchange name="
						+ rabbit_exchangeName
						+ ", on "
						+ rabbitMQServerIp
						+ ":" + rabbit_port);
	}

	/**
	 * Checks a given string whether it is a valid number or not. Else, returns
	 * -1.
	 *
	 * @param configuration
	 * @return a port number that defines either custom port number (non
	 *         negative) or default port (-1 in this context)
	 */
	private int GetPort(String portField) {
		int retVal = -1;

		if (isNullOrEmpty(portField) == false) {
			try {
				retVal = Integer.parseInt(portField);
			} catch (Exception ex) {
				// no need to handle anything.
			}
		}

		return retVal;
	}

	@Override
	public void stop() {
		super.stop();
		((RabbitMQSender) handler).DisconnectFromExchange();
	}

}
