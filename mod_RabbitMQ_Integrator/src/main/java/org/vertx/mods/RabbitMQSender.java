package org.vertx.mods;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.io.IOException;
import java.net.ConnectException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitMQSender implements Handler<Message<JsonObject>> {

	private static final Logger LOG = LoggerFactory
			.getLogger(RabbitMQSender.class);

	private String rabbit_exchangeName = null;// name of the exchange used
												// in
	// RabitMQ.
	private int rabbit_port = -1;// -1 for default
	private String rabbitMQServerIp = null;
	private String rabbit_exchangeType = null;

	private String topicApplication = null;// value in json for this field
											// os sensorcloud

	private ConnectionFactory factory = null;
	private Connection connection = null;
	private Channel channel = null;

	public RabbitMQSender(String rabbitMQServerIp, int rabbit_port,
			String rabbit_exchangeName, String rabbit_exchangeType,
			String topicApplication) {
		super();
		checkArgument(!isNullOrEmpty(rabbit_exchangeName));
		checkArgument(!isNullOrEmpty(rabbitMQServerIp));
		checkArgument(!isNullOrEmpty(topicApplication));
		
		this.rabbit_exchangeName = rabbit_exchangeName;
		this.rabbit_port = rabbit_port;
		this.rabbitMQServerIp = rabbitMQServerIp;
		this.rabbit_exchangeType = rabbit_exchangeType;
		this.topicApplication = topicApplication;
		LOG.debug("RabbitMQSender initialization completed");
	}

	@Override
	public void handle(Message<JsonObject> message) {
		try {

			if (connection == null || channel == null) {
				ConnectToExchange();
			}

			// process the message received from vertx event Bus
			JsonObject messageBody = message.body();
			publish(channel, messageBody);

		} catch (IOException e) {
			message.fail(-1, e.getMessage());
		}catch (TimeoutException e) {
			message.fail(-1, "Error while trying to connect to the rabbitMQ exchange");
		}
	}

	private void ConnectToExchange() throws IOException, TimeoutException {
		// configure connectionFactory
		factory = new ConnectionFactory();
		factory.setHost(rabbitMQServerIp);

		checkArgument(!isNullOrEmpty(rabbit_exchangeType));
		checkArgument(!isNullOrEmpty(rabbitMQServerIp));

		if (rabbit_port != -1)
			factory.setPort(rabbit_port);
		// configure connectionFactory

		// Create a channel
		connection = factory.newConnection();
		channel = connection.createChannel();
		channel.exchangeDeclare(rabbit_exchangeName, rabbit_exchangeType);
		// Create a channel
	}

	void DisconnectFromExchange() {

		try {
			if (connection != null)
				connection.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void publish(Channel channel, JsonObject messageBody) throws IOException{

		checkArgument(messageBody != null);
		
		String deviceId = messageBody.getString("id");
		String tenantId = messageBody.getString("tenantId");

		checkArgument(!isNullOrEmpty(deviceId));

		// logically construct a routing key. Followed the example in
		// MqttChannel.java
		String routingKey = topicApplication + "." + tenantId + "." + deviceId;

		/*
		 * Topic exchange is powerful and can behave like other exchanges. When
		 * a queue is bound with "#" (hash) binding key - it will receive all
		 * the messages, regardless of the routing key - like in fanout
		 * exchange.
		 * 
		 * When special characters "*" (star) and "#" (hash) aren't used in
		 * bindings, the topic exchange will behave just like a direct one.
		 */

		/*
		 * Note for david(or other receivers): When you bind to the channel, ex:
		 * channel.queueBind(queueName, EXCHANGE_NAME, bindingKey); Give
		 * bindingKey="#" if you want to recieve all messages (typical fanout
		 * kind of publish & subscribe)
		 */
		byte[] yourData = messageBody.toString().getBytes(
				Charset.forName("UTF-8"));
		channel.basicPublish(rabbit_exchangeName, routingKey, null, yourData);// 3rd
		// param can hold other properties for the message - routing headers etc

		LOG.info("Message sent with key - " + routingKey);

	}

}