package org.vertx.mods;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.vertx.java.core.json.JsonObject;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class RabbitMQSenderTest {

	@Test
	public void Input_SanityTest1() {
		RabbitMQSender handler = new RabbitMQSender("1111", 1, "exchaneName",
				"topic", "appTopic");

		Channel channel = mock(Channel.class);

		JsonObject message = new JsonObject();
		message.putString("id", "testID");
		message.putString("tenantId", "testTenant");
		message.putString("body", "does_not_matter");

		try {
			handler.publish(channel, message);

			verify(channel).basicPublish("exchaneName",
					"appTopic.testTenant.testID", null,
					message.toString().getBytes(Charset.forName("UTF-8")));
		} catch (Exception ex) {
			fail("Not yet implemented");
		}

	}

	@Test
	public void RabbitMQSender_InvalidArgumentTest() {
		RabbitMQSender handler = new RabbitMQSender("1111", -1, "exchaneName",
				"topic", "appTopic");

		Channel channel = mock(Channel.class);

		JsonObject message = new JsonObject();
		message.putString("id", "testID");
		message.putString("tenantId", "testTenant");
		message.putString("body", "hjdsks");

		try {
			handler.publish(channel, message);

			verify(channel).basicPublish("exchaneName",
					"appTopic.testTenant.testID", null,
					message.toString().getBytes(Charset.forName("UTF-8")));
		} catch (Exception ex) {
			fail("Not yet implemented");
		}
	}

	@Test
	public void Publish_NullArgumentTest() {
		RabbitMQSender handler = new RabbitMQSender("1111", -1, "exchaneName",
				"topic", "appTopic");

		Channel channel = mock(Channel.class);

		try {
			handler.publish(channel, null);
			fail("Expecting an exception which was not thrown.");

		} catch (IllegalArgumentException ex) {
			assertTrue(
					"It is expected that publish method throws an argument exception if message is null",
					true);
		} catch (Exception ex) {
			assertTrue("Unexpected exception", false);
		}
	}

	@Test
	public void InvalidInitialize_Test() {
		try {
			RabbitMQSender handler = new RabbitMQSender("1111", -1, null, null,
					null);
			fail("Expecting an exception which was not thrown.");

		} catch (IllegalArgumentException ex) {
			assertTrue(
					"Constructor is expected to throw an exception if exchange name,type or topicApplication is null",
					true);
		}
	}

	@Test
	public void ConnectToExchange_Test1() {
		try {
			RabbitMQSender handler = new RabbitMQSender("localhost", -1,
					"test", "topic", "myApp");

			// using reflection to trigger private method
			TriggerConnect(handler);

			handler.DisconnectFromExchange();

		} catch (Exception ex) {
			assertTrue(
					"Invoking ConnectToExchange() method should have happened smoothly",
					false);
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void ConnectToExchange_InternalInitializationTest() {
		try {
			RabbitMQSender handler = new RabbitMQSender("localhost", -1,
					"test", "topic", "myApp");

			// using reflection to trigger private method
			TriggerConnect(handler);

			List<String> attributeNames = new ArrayList<String>();
			attributeNames.add("connection");
			attributeNames.add("channel");
			attributeNames.add("factory");

			attributeNames
					.stream()
					.forEach((attrName) -> {

						// using reflection to check if private members are not
						// null

							try {
								Field field = RabbitMQSender.class
										.getDeclaredField(attrName);
								field.setAccessible(true);
								Object value = field.get(handler);
								if (value == null)
									assertTrue(
											"Attribute "
													+ attrName
													+ " is still null after ConnectToExchange is called",
											false);

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						});

			handler.DisconnectFromExchange();

		} catch (Exception ex) {
			assertTrue(
					"Invoking ConnectToExchange() method should have happened smoothly",
					false);
		}
	}

	@Test
	public void CrossCheck_ReceiverTest1() {
		try {
			String serverIP = "localhost";
			int port = -1;
			String exchangeName = "test1";
			String topicApplication = "myApp";
			String exchangeType = "topic";

			RabbitMQSender handler = new RabbitMQSender("localhost", -1,
					exchangeName, exchangeType, topicApplication);
			// using reflection to trigger private method
			TriggerConnect(handler);

			// extract channel object
			Channel senderChannel = GetChannel(handler);
			if (senderChannel == null) {
				fail("Channel cannot be null");
				handler.DisconnectFromExchange();
				return;
			}

			// implementing the receiver
			QueueingConsumer consumer = createConsumer(serverIP, exchangeName,
					exchangeType, "#");

			// send message
			JsonObject jsonMsg = new JsonObject();
			jsonMsg.putString("id", "testID");
			jsonMsg.putString("tenantId", "testTenant");
			jsonMsg.putString("body", "hjdsks");
			handler.publish(senderChannel, jsonMsg);
			handler.DisconnectFromExchange();

			boolean hasReceived = false;
			String messageReceived = null;
			String routingKey = null;
			while (hasReceived == false) {
				QueueingConsumer.Delivery delivery = consumer.nextDelivery();
				messageReceived = new String(delivery.getBody());
				routingKey = delivery.getEnvelope().getRoutingKey();
				hasReceived = true;
			}
			assertEquals("myApp.testTenant.testID", routingKey);

		} catch (Exception ex) {
			fail(ex.getMessage());
		}

	}

	@Test
	public void BindingCrossCheck_ReceiverTest1_Withtimeout() {
		try {
			String serverIP = "localhost";
			int port = -1;
			String exchangeName = "test1";
			String topicApplication = "myApp";
			String exchangeType = "topic";
			int receiverTimeout = 100;// in milli seconds
			RabbitMQSender handler = new RabbitMQSender("localhost", -1,
					exchangeName, exchangeType, topicApplication);
			// using reflection to trigger private method
			TriggerConnect(handler);

			// extract channel object
			Channel senderChannel = GetChannel(handler);
			if (senderChannel == null) {
				fail("Channel cannot be null");
				handler.DisconnectFromExchange();
				return;
			}

			String tenantID = "ten1";

			// implementing the receiver
			QueueingConsumer consumer = createConsumer(serverIP, exchangeName,
					exchangeType, tenantID + ".*");

			// send messages
			int sendingCount = 10;

			for (int i = 0; i < sendingCount; i++) {
				JsonObject jsonMsg = new JsonObject();
				jsonMsg.putString("id", "testID");
				jsonMsg.putString("tenantId", tenantID);
				jsonMsg.putString("body", "timeoutExample");
				handler.publish(senderChannel, jsonMsg);
			}

			handler.DisconnectFromExchange();

			int receivedCount = 0;
			while (receivedCount < sendingCount) {
				QueueingConsumer.Delivery delivery = consumer.nextDelivery(receiverTimeout);
				receivedCount++;
			}
			assertEquals(sendingCount, receivedCount);

		} catch (Exception ex) {
			fail(ex.getMessage());
		}

	}

	private void TriggerConnect(RabbitMQSender handler)
			throws NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		Method method = RabbitMQSender.class.getDeclaredMethod(
				"ConnectToExchange", null);
		method.setAccessible(true);
		method.invoke(handler, null);

	}

	private QueueingConsumer createConsumer(String serverIP,
			String exchangeName, String exchangeType, String binding)
			throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(serverIP);
		Connection connection = factory.newConnection();
		Channel receiver_channel = connection.createChannel();

		receiver_channel.exchangeDeclare(exchangeName, exchangeType);
		String queueName = receiver_channel.queueDeclare().getQueue();
		receiver_channel.queueBind(queueName, exchangeName, binding);

		QueueingConsumer consumer = new QueueingConsumer(receiver_channel);
		receiver_channel.basicConsume(queueName, true, consumer);
		return consumer;
	}

	private Channel GetChannel(RabbitMQSender handler) {
		try {
			Field field = RabbitMQSender.class.getDeclaredField("channel");
			field.setAccessible(true);
			return (Channel) field.get(handler);
		} catch (Exception ex) {
			return null;
		}
	}
}
