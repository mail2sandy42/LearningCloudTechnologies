package AMQP;

import java.nio.charset.Charset;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitMQChannel extends Verticle {

	private Handler<Message<JsonObject>> handler = this::handle;
	private String topicApplication = null;
	private String exchangeName = null;

	@Override
	public void start() {
		vertx.eventBus().registerHandler("someTopicName", handler);
		topicApplication = "topicApplication";
		// topicApplication = config.getString( CFG_TOPIC_OUT_APPLICATION );

		String exchangeName = "readFromConfigFile";
	}

	public void handle(Message<JsonObject> message) {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("10.0.0.10");
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			
			channel.exchangeDeclare(exchangeName, "topic");

			JsonObject messageBody = message.body();

			String deviceId = messageBody.getString("id");
			String tenantId = messageBody.getString("tenantId");
			
			// String routingKey = "CurrentlyAll";//but logically construct this
			// of exchange is topic and not direct
			String routingKey = topicApplication + "." + tenantId + "."
					+ deviceId;//try to follow example in MqttChannel.java

			/*
			 * Topic exchange is powerful and can behave like other exchanges.
			 * When a queue is bound with "#" (hash) binding key - it will
			 * receive all the messages, regardless of the routing key - like in
			 * fanout exchange.
			 * 
			 * When special characters "*" (star) and "#" (hash) aren't used in
			 * bindings, the topic exchange will behave just like a direct one.
			 */

			/*Note for david(or other receivers):
			 * When you bind to the channel, ex: channel.queueBind(queueName, EXCHANGE_NAME, bindingKey);
			 * Give bindingKey="#" if you want to recieve all messages (typical fanout kind of publish & subscribe)
			 * 
			 * */
			byte[] yourData = messageBody.toString().getBytes(
					Charset.forName("UTF-8"));
			channel.basicPublish(exchangeName, routingKey, null, yourData);
			System.out.println(" [x] Sent '" + routingKey + "':'" + message
					+ "'");

			connection.close();

		} catch (Exception e) {
			// what do you usually do here?
		}
	}

}
