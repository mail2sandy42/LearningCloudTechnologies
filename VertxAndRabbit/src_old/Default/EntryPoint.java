package Default;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.VertxFactory;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.PlatformLocator;
import org.vertx.java.platform.PlatformManager;

public class EntryPoint{

	
	public static void main(String[] args) {
		// Start your verticle here. Dont worry. it doesnt create multiple instances of same verticle.
			
		PlatformManager pm = PlatformLocator.factory.createPlatformManager();
	      //Once you have an instance of PlatformManager you can deploy and undeploy modules and verticles, install modules and various other actions
		JsonObject conf = new JsonObject().putString("foo", "wibble");
		Vertx vertx = VertxFactory.newVertx();
		
		/*pm.deployVerticle("RabbitMQChannel", arg1, conf, arg3, 2, new AsyncResultHandler<String>() {
		    public void handle(AsyncResult<String> asyncResult) {
		        if (asyncResult.succeeded()) {
		            System.out.println("Deployment ID is " + asyncResult.result());
		        } else {
		            asyncResult.cause().printStackTrace();
		        }
		    }
		});*/
		
		
		//Deploy 10 instances of a module passing in some config
		/*pm.deployModule("AMQP.RabbitMQChannel", conf, 10, new AsyncResultHandler<String>() {
		    public void handle(AsyncResult<String> asyncResult) {
		        if (asyncResult.succeeded()) {
		            System.out.println("Deployment ID is " + asyncResult.result());
		        } else {
		            asyncResult.cause().printStackTrace();
		        }
		    }
		});*/

	}

}
